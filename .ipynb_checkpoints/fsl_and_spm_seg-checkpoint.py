import yaml
from nipype import Node, Workflow
from nipype.interfaces import fsl
import nipype.interfaces.spm as spm
from nipype.interfaces.utility import Merge
from nipype.algorithms.misc import Gunzip
from IPython.display import SVG, Image

config_yaml = '/home/neuro/code/config-EBM05.yml'
config_json = '/home/neuro/code/config-bids-EBM05.json'
config = yaml.safe_load(open(config_yaml))

bet = Node(fsl.BET(), name = 'bet')
bet.inputs.in_file = config['PATH']['data'] + '/' + config['PROJECT'] + config['PATH']['T1w'] + '/_session_id_01_subject_id_01/sub-01_ses-01_run-01_T1w_reoriented.nii'
bet.inputs.mask = True

applymask = Node(fsl.maths.ApplyMask(), name = 'applymask')

flirt_t2_t1 = Node(fsl.FLIRT(), name = 'flirt_t2_t1')
flirt_t2_t1.inputs.in_file = config['PATH']['data'] + '/' + config['PROJECT'] + config['PATH']['T2w'] + '/_session_id_01_subject_id_01/sub-01_ses-01_run-01_T2w_reoriented.nii'
flirt_t2_t1.inputs.reference = config['PATH']['data'] + '/' + config['PROJECT'] + config['PATH']['T1w'] + '/_session_id_01_subject_id_01/sub-01_ses-01_run-01_T1w_reoriented.nii'
flirt_t2_t1.inputs.dof = 6
flirt_t2_t1.inputs.cost = 'mutualinfo'

flirt_t1_f99 = Node(fsl.FLIRT(), name = 'flirt_t1_f99')
flirt_t1_f99.inputs.in_file = config['PATH']['data'] + '/' + config['PROJECT'] + config['PATH']['T1w'] + '/_session_id_01_subject_id_01/sub-01_ses-01_run-01_T1w_reoriented.nii'
flirt_t1_f99.inputs.reference = config['PATH']['template']['T1w']

flirt_t2_f99 = Node(fsl.FLIRT(), name = 'flirt_t2_f99')
flirt_t2_f99.inputs.in_file = config['PATH']['data'] + '/' + config['PROJECT'] + config['PATH']['T2w'] + '/_session_id_01_subject_id_01/sub-01_ses-01_run-01_T2w_reoriented.nii'
flirt_t2_f99.inputs.reference = config['PATH']['template']['T2w']

merge = Node(Merge(2), name = 'merge')

fast = Node(fsl.FAST(), name = 'fast')
fast.inputs.other_priors = [config['PATH']['template']['GM'], config['PATH']['template']['WM'], config['PATH']['template']['CSF']]
#fast.iterables = ("hyper", [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])

fast_with_t2 = Node(fsl.FAST(), name = 'fast_w_t2')
#fast_with_t2.iterables = ("hyper", [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])

gunzip = Node(Gunzip(), name = 'gunzip')

seg = Node(spm.Segment(), name = 'seg')
seg.inputs.gm_output_type = [False, False, True]
seg.inputs.wm_output_type = [False, False, True]
seg.inputs.csf_output_type = [False, False, True]
seg.inputs.save_bias_corrected = True
seg.inputs.clean_masks = 'no'
seg.inputs.tissue_prob_maps= [config['PATH']['template']['GM'], config['PATH']['template']['WM'], config['PATH']['template']['CSF']]
seg.inputs.gaussians_per_class = [2, 2, 2, 4]
seg.inputs.affine_regularization = 'subj'
seg.inputs.warping_regularization = 1
seg.inputs.bias_regularization = 1e-5
seg.inputs.bias_fwhm = 30
seg.inputs.sampling_distance = 3

wf = Workflow(name = 'flow', base_dir = '/home/neuro/output')

wf.connect([
  (bet, merge, [('out_file', 'in1')]),
  (flirt_t2_t1, applymask, [('out_file', 'in_file')]),
  (bet, applymask, [('mask_file', 'mask_file')]),
  (applymask, flirt_t2_f99, [('out_file', 'in_file')]),
  (flirt_t2_f99, merge, [('out_file', 'in2')]),
  (merge, fast_with_t2, [('out', 'in_files')]),
  (bet, flirt_t1_f99, [('out_file', 'in_file')]),
  (flirt_t1_f99, fast, [('out_file', 'in_files')]),
  (flirt_t1_f99, gunzip, [('out_file', 'in_file')]),
  (gunzip, seg, [('out_file', 'data')])
])

# Visualize the workflow
wf.write_graph(graph2use='colored', format='png', simple_form=False)

Image(filename=config['PATH']['output'] + '/flow/graph.png', width=250)

wf.run('MultiProc', plugin_args={'n_procs': 2})