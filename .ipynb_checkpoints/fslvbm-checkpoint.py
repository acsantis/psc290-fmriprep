#!/usr/bin/env python
# coding: utf-8

# get the Node, Workflow, SelectFiles, and MapNode objects from the NiPype
# package
import glob
import pandas as pd
import yaml
from shutil import copyfile
from pypelinefuncs.workflows import vbm_template_flow
from pypelinefuncs.fslorient import FslOrient
from nipype.interfaces.freesurfer import MRIConvert
from nipype.interfaces.fsl.maths import MathsCommand
from nipype.interfaces.fsl import SwapDimensions
from nipype.interfaces import fsl
from nipype.interfaces import utility as niu
from nipype import Node, Workflow, SelectFiles, DataSink, MapNode

# import other general packages, such as nibabel
from IPython.display import SVG, Image
import nibabel as nb
import numpy as np
import bids.layout

def affine_gm_to_std_temp(path_ref, dir_subj, dir_out, subjects):
    """ Affine register GM masks to standard template
    Affine registers already GM segmented images. Images were segmented through
      Dante's spm prep script.

    Arguments:
        - path_ref: str
            Path to the reference .nii for FLIRT.
        - dir_subj: str
            Path to the directory containing the subject data.
        - dir_out: str
            Path to the directory where the outputs will be generated.
        - subjects: str
            List of subjects whose data will be processed.

    """

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    gm_images = {'GM': '_subject_id_{subject}/sub-{subject}_ses-01_run-01_T1w_reorientedt.nii'}

    sf = Node(SelectFiles(gm_images,
                          base_directory=dir_subj,
                          sort_filelist=True),
              name='selectfiles')
    sf.iterables = [('subject', subjects)]

    flirt_wf = Workflow(name='flirt', base_dir=dir_out)

    bet = Node(fsl.BET(), name = 'bet')

    fast = Node(fsl.FAST(), name = 'fast')

    flirt = Node(fsl.FLIRT(), name='flirt')
    flirt.inputs.reference = path_ref

    select_1 = Node(niu.Select(index=1), name='select_1')

    flirt_wf.connect([
        (sf, bet, [('GM', 'in_file')]),
        (bet, fast, [('out_file', 'in_files')]),
        (fast, select_1, [("partial_volume_files", 'inlist')]),
        (select_1, flirt, [("out", 'in_file')]),
    ])


    # Visualize the workflow
    flirt_wf.write_graph(graph2use='colored',
                         format='png',
                         simple_form=False)

    Image(filename=dir_out + 'flirt/graph.png', width=250)
    flirt_wf.run()


def create_first_study_template(dir_base):
    """ Create first study template.
    Create first study template by averaging, mirroring across x-axis, and
      averaging the original and mirrored images.

    Arguments:
        - dir_base: str
            The base directory for VBM template. This should also be the
            directory where FLIRT generated its output.
    """
    # get all of the flirted images
    pattern = dir_base + 'flirt/_subject_*/flirt/c1structural3D_flirt.nii.gz'
    GM_flirt = glob.glob(pattern)

    vbm_template = vbm_template_flow(name='vbm_template',
                                     in_files=GM_flirt,
                                     base_dir=dir_base)

    # Visualize the workflow
    vbm_template.write_graph(graph2use='colored',
                             format='png',
                             simple_form=False)

    Image(filename=dir_base + 'vbm_template/graph.png', width=250)
    vbm_template.run()

def nonlinear_reg_to_first(dir_ref, dir_subj, path_fnirt_config, subjects):
    """ Nonlinearly register each GM segmentation to the first template.

    Arguments:
        - dir_ref: str
            Directory that contains the VBM templates and FLIRT outputs which
            will be passed into FNIRT.
        - dir_subj: str
            Directory that contains the subject data.
        - path_fnirt_config: str
            Path to the FNIRT configuration file.
        - subjects: list
            List of subjects whose data to be processed.

    """
    fnirt_wf = Workflow(name='fnirt_initial_template', base_dir=dir_ref)
    fnirt = Node(fsl.FNIRT(), name='fnirt')
    fnirt.inputs.ref_file = dir_ref + \
        'vbm_template/template/c1structural3D_flirt_merged_maths_maths.nii.gz'
    fnirt.inputs.config_file = path_fnirt_config
    gm_images = {'GM': dir_ref +
                 'flirt/_subject_{subject}/flirt/c1structural3D_flirt.nii.gz'}

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    fnirt_sf = Node(SelectFiles(gm_images,
                                base_directory=dir_subj,
                                sort_filelist=True),
                    name='fnirt_selectfiles')
    fnirt_sf.iterables = [('subject', subjects)]
    fnirt_wf.connect([
        (fnirt_sf, fnirt, [('GM', 'in_file')])])

    fnirt_wf.run()


def create_final_study_template(dir_out):
    """ Create the final study template.
    Create final study template by averaging nonlinearly registered GM images,
        mirroring across x-axis, and averaging with the mirrored image.

    Arguments:
        - dir_out: str
            Path to the directory that contains FNIRT outputs and VBM templates.
    """
    # get all of the flirted images
    GM_fnirt = glob.glob(
        dir_out +
        'fnirt_wf/_subject_*/fnirt/c1structural3D_flirt_warped.nii.gz')
    GM_fnirt.append(
        dir_out +
        'vbm_template/template/c1structural3D_flirt_merged_maths_maths.nii.gz')

    vbm_final_template = vbm_template_flow(
        name='vbm_final_template',
        in_files=GM_fnirt,
        base_dir=dir_out)

    # Visualize the workflow
    vbm_final_template.write_graph(graph2use='colored',
                                   format='png',
                                   simple_form=False)
    Image(filename=dir_out+ 'vbm_final_template/graph.png', width=250)
    vbm_final_template.run()


def nonlinear_register_to_final(dir_out, path_fnirt_config, subjects):
    """ Nonlinearly register each GM segmentation to the final nonlinear
        template.
    Save the jacobians and modulate the GM images by the jacobians, and smooth
        them.
    Arguments:
        - dir_out: str
            Path to the directory that contains FNIRT outputs and VBM templates.
        - path_fnirt_config: str
            Path to the FNIRT configuration file.
        - subjects: list
            List of subjects whose data to be processed.
    """
    fnirt_wf_2 = Workflow(name='fnirt_final_template', base_dir=dir_out)
    fnirt_2 = Node(fsl.FNIRT(), name='fnirt2')
    fnirt_2.inputs.ref_file = dir_out + \
        'vbm_final_template/template/' + \
        'c1structural3D_flirt_merged_maths_maths_merged_maths_maths.nii.gz'
    fnirt_2.inputs.jacobian_file = True
    fnirt_2.inputs.config_file =path_fnirt_config
    fnirt_2.inputs.refmask_file = dir_out + \
        'vbm_final_template/dil/' + \
        'c1structural3D_flirt_merged_maths_maths_merged_maths_maths_maths_' + \
        'maths.nii.gz'

    multiply = Node(fsl.MultiImageMaths(), name='modulate')
    multiply.inputs.op_string = '-mul %s'

    smooth = Node(fsl.IsotropicSmooth(), name='smooth')
    smooth.inputs.fwhm = 3

    gm_images = {'GM': dir_out +
                 'flirt/_subject_{subject}/flirt/c1structural3D_flirt.nii.gz'}

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    fnirt_sf2 = Node(SelectFiles(gm_images,
                                 base_directory=dir_out,
                                 sort_filelist=True),
                     name='fnirt_selectfiles_2')
    fnirt_sf2.iterables = [('subject', subjects)]
    fnirt_wf_2.connect([
        (fnirt_sf2, fnirt_2, [('GM', 'in_file')]),
        (fnirt_2, multiply, [('warped_file', 'in_file')]),
        (fnirt_2, multiply, [('jacobian_file', 'operand_files')]),
        (multiply, smooth, [('out_file', 'in_file')])
    ])
    fnirt_wf_2.run()

def main():
    # loads up configuration files
    config_yaml = '/home/neuro/code/config-EBM05.yml'
    config = yaml.safe_load(open(config_yaml))
    path = config['PATH']

    affine_gm_to_std_temp(path_ref = path['template']['GM_LR'],
                          dir_subj = path['data'],
                          dir_out = path['output'],
                          subjects = config['subjects'])

    create_first_study_template(dir_base = path['output'])

    nonlinear_reg_to_first(dir_ref = path['output'],
                           dir_subj = path['data'],
                           path_fnirt_config = path['fnirt']['CNF'],
                           subjects = config['subjects'])

    create_final_study_template(dir_out = path['output'])

    nonlinear_register_to_final(dir_out = path['output'],
                                path_fnirt_config = path['fnirt']['CNF'],
                                subjects = config['subjects'])

if __name__ == '__main__':
    main()
