#!/bin/bash

docker run --rm kaczmarj/neurodocker:master generate docker --base neurodebian:nd16.04 \
           --pkg-manager apt \
           --install convert3d ants fsl gcc g++ graphviz tree \
                     git-annex-standalone vim emacs-nox nano less ncdu \
                     tig git-annex-remote-rclone octave \
           --add-to-entrypoint "source /etc/fsl/fsl.sh" \
           --ants version=2.3.1 \
           --spm12 version=r7219 \
           --freesurfer version=6.0.0 method=binaries \
           --dcm2niix version=latest method=source \
           --user=neuro \
           --miniconda miniconda_version="4.3.31" \
             conda_install="python=3.6 pytest jupyter jupyterlab jupyter_contrib_nbextensions
                            traits pandas matplotlib scikit-learn scikit-image seaborn nbformat nb_conda" \
             pip_install="https://github.com/nipy/nipype/tarball/master
                          https://github.com/INCF/pybids/tarball/master
                          nilearn datalad[full] nipy duecredit nbval Snakemake dcm2bids pydicom" \
             create_env="neuro" \
             activate=True \
           --run-bash "source activate neuro && jupyter nbextension enable exercise2/main && jupyter nbextension enable spellchecker/main" \
           --user=root \
	         --run 'mkdir /home/neuro/data && chmod 777 /home/neuro/data && chmod a+s /home/neuro/data' \
           --run 'mkdir /home/neuro/code && chmod 777 /home/neuro/code && chmod a+s /home/neuro/code' \
           --run 'mkdir /home/neuro/output && chmod 777 /home/neuro/output && chmod a+s /home/neuro/output' \
           --user=root \
           --run 'rm -rf /opt/conda/pkgs/*' \
           --copy fs-license.txt /opt/freesurfer-6.0.0/license.txt \
           --user=neuro \
           --run 'mkdir -p ~/.jupyter && echo c.NotebookApp.ip = \"0.0.0.0\" > ~/.jupyter/jupyter_notebook_config.py' \
           --workdir /home/neuro \
           --cmd "jupyter-lab" > Dockerfile
