# coding: utf-8

# get the Node, Workflow, SelectFiles, and MapNode objects from the NiPype package 
from nipype import Node, Workflow, SelectFiles, DataSink, MapNode
from nipype.interfaces import fsl

# import other general packages, such as nibabel 
from IPython.display import SVG, Image
import nibabel as nb
import numpy as np
import bids.layout

# ignore ugly wargnings because ugly
import warnings
warnings.filterwarnings('ignore')

# yaml for reading in configs
import yaml

# loads up configuration files
config_yaml = '/home/neuro/code/config-EST06.yml'
config = yaml.safe_load(open(config_yaml))
from nipype.interfaces.fsl import SwapDimensions, BET
from pypelinefuncs.fslorient import FslOrient
from nipype.interfaces.freesurfer import MRIConvert
from nipype.algorithms.misc import Gunzip
from pypelinefuncs.extractmetadata import GetFieldFromSidecar
from nipype.interfaces.spm import SliceTiming, NewSegment
import nipype.interfaces.spm as spm
import nibabel as nib
# import workflow
from pypelinefuncs.workflows import create_reorient_flow
from pypelinefuncs.workflows import create_topup_flow, mriconvert_flow
from nipype.interfaces import freesurfer

path = config['PATH']


Reorient = Workflow(name=path['preprocessed_output'], base_dir=path['output'])

# Datainput with SelectFiles and iterables
# this will search for files that match this structure, so any BIDS files can be selected. 
# This uses the string formatting syntax to plug values into string templates, and collect files
# these can also be combined with glob wildcards. The field names (i.e. the terms in braces)
# will become inputs fields and keys in the dictionary will form output
templates = {'T1w': 'sub-{subject_id}/ses-{session_id}/anat/*_T1w.nii.gz',
             #'T2w': 'sub-{subject_id}/ses-{session_id}/anat/*_T2w.nii.gz',
             'func_1': 'sub-{subject_id}/ses-{session_id}/func/*rest_run-01_bold.nii',
             'func_2': 'sub-{subject_id}/ses-{session_id}/func/*rest_run-02_bold.nii'}


# create SelectFiles node
# This node uses the new templates to find files. Below, we added a call to an
# external .yml file, which automates selection of appropriate files 
sf = Node(SelectFiles(templates,
                      base_directory=path["bids_data"],
                      sort_filelist=True),
          name='selectfiles')


T1w_reorient = mriconvert_flow(in_orientation = "LIA", out_orientation = "LAS", name = "T1w_reorient")

func_1_reorient = mriconvert_flow(in_orientation = "RAS", out_orientation = "LAS", name = "func_1_reorient")

func_2_reorient = mriconvert_flow(in_orientation = "RAS", out_orientation = "LAS", name = "func_2_reorient")

bet_T1w = Node(BET(), name='bet_T1w')

gunzip_func_1 = Node(Gunzip(), name='gunzip_func_1')
gunzip_func_2 = Node(Gunzip(), name='gunzip_func_2')

bet_func = Node(BET(), name='bet_func')
bet_func.inputs.args = '-F'

topup = create_topup_flow()

# create datasink output node
datasink_T1w = Node(DataSink(), name='anat_sinker')
datasink_T1w.inputs.base_directory = path['output'] + '/' + path['preprocessed_output']

datasink_T2w = Node(DataSink(), name='t2_sinker')
datasink_T2w.inputs.base_directory = path['output'] + '/' + path['preprocessed_output']

datasink_func1 = Node(DataSink(), name='func_sinker1')
datasink_func1.inputs.base_directory = path['output'] + '/' + path['preprocessed_output']

datasink_func2 = Node(DataSink(), name='func_sinker2')
datasink_func2.inputs.base_directory = path['output'] + '/' + path['preprocessed_output']

datasink_func_merged = Node(DataSink(), name='func_sinker_merged')
datasink_func_merged.inputs.base_directory = path['output'] + '/' + path['preprocessed_output']

# Specify over which subjects the workflow should iterate.

subject_list = config['subjects']
sessions_list = config['sessions']

sf.iterables = [('session_id', sessions_list), ('subject_id', subject_list)]

Reorient.connect([
    (sf, T1w_reorient, [('T1w', 'inputnode.in_file')]),
    (sf, func_1_reorient, [('func_1', 'inputnode.in_file')]),
    (sf, func_2_reorient, [('func_2', 'inputnode.in_file')]),
    (T1w_reorient, bet_T1w, [('outputnode.reoriented', 'in_file')]),
    (bet_T1w, datasink_T1w, [('out_file', 'reoriented_T1w')]),
    (func_1_reorient, topup, [('outputnode.reoriented', 'inputnode.in_file1')]),
    (func_2_reorient, topup, [('outputnode.reoriented', 'inputnode.in_file2')]),
    (topup, bet_func, [('outputnode1.corrected', 'in_file')]),
    (bet_func, datasink_func1, [('out_file', 'topup_corrected')]),
    (topup, datasink_func2, [('outputnode2.corrected', 'topup_corrected')]),
    (topup, datasink_func_merged, [('outputnode3.merged_corrected', 'topup_corrected')])
])


# Visualize the workflow
Reorient.write_graph(graph2use='colored',
                    format='png',
                    simple_form=True)

Image(filename=path['output'] + '/' + path['preprocessed_output'] +
      '/' + 'graph.png', width=450)


#Run the Workflow

### NOTE ### 
# be careful about the n_procs parameter if you run a workflow in 'MultiProc' mode 
# n_procs specifies the number of jobs/cores your computer will use
# and if this number is too high, your computer is attempting to run too much, and will most likely crash!

Reorient.run('MultiProc', plugin_args={'n_procs': config["n_procs"]})
