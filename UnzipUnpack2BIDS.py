# coding: utf-8

from collections import OrderedDict
import nibabel as nib
import glob
from os.path import abspath
import json
import pandas as pd
import pydicom
from pypelinefuncs.extractmetadata import GetFieldFromDicom
from pypelinefuncs.unzip import UnzipSubDir
import re
import yaml
import numpy as np
from nipype.interfaces.freesurfer import UnpackSDICOMDir
import os
from re import findall
import os

def MakeBidsPath(sub, ses):
    return('sub-' + sub + '/' + 'ses-' + ses)

def MakeBidsT1wFile(sub, ses, run):
    return('sub-' + sub + '_ses-' + ses + '_run-' + run + '_T1w.nii.gz')

def MakeBidsT2wFile(sub, ses, run):
    return('sub-' + sub + '_ses-' + ses + '_run-' + run + '_T2w.nii.gz')

def MakeBidsFuncFile(sub, ses, task, run):
    return('sub-' + sub + '_ses-' + ses + '_task-' + task + '_run-' + run + '_bold.nii.gz')


# config-pipeline.yml contains all of the conguration parameters for the pipeline
config_yaml = '/home/neuro/code/config-EST06.yml'
config_json = '/home/neuro/code/config-bids-EST06.json'

# Load configuration file
config = yaml.safe_load(open(config_yaml))

# Generates list of MRI image types to unzip. Here, we are only going to
# unzip T1-weighted and BOLD images from the raw dicoms
images_to_unzip = [config['images']['T1']['regex'], config['images']['T2']['regex'], config['images']['func']['regex']]

# Finds all zipped files in zip_dir
zipped_files = glob.glob(abspath(config['PATH']['zipped_dicoms']) + '/*.zip')

# Unzips all the .zip dicom folders
for f in zipped_files:
    print(f)
    #UnzipSubDir(f, images_to_unzip, config['PATH']['zipped_dicoms'])

# Gets first volume from each directory matching the regex
files = glob.glob(config['PATH']['zipped_dicoms'] + '/*/*' + config['images']['T1']['regex'] + '*/0001.dcm', recursive=False)
# Get just the scan/directory name
scans = [str.split(i, "/")[5] for i in files]

# Extract PatientName field from the first dicom image for each participant
subject = [re.sub('\D', '', str(GetFieldFromDicom(i, 'PatientName'))) for i in files]

# Stores the subject name and the scan identifier for each session in a dataframe
# and subsequently drops any duplicates
df = pd.DataFrame({'subject': subject, 'scan': scans})
df = df.drop_duplicates()

# Here we get the unique subjects from the all of the 
# extracted subjects by converting them to an ordered dictionary.
# Using the OrderedDict function preserves the order of the subjects, which
# is important for maintaining the mapping between subjects and scans. We
# then convert the OrderedDict back to an iterable list.
unique_subject = list(OrderedDict.fromkeys(subject))
# Computes a new subject id (ranging from 1,... N) and formats the numbers to have
# at least two digits; i.e., 1, 2, 3, ..., N becomes 01, 02, 03, ... N.
sub_id = ['{0:0>2}'.format(i) for i in range(1, len(unique_subject) + 1)]
subj_ids = pd.DataFrame({'subject': unique_subject, 'participant_id' : sub_id})

# Merges the newly computed subject ids in with the PatientNames and scan ids.
df2 = df.merge(subj_ids, how='left')

# Computes and formats session value by grouping by subject
# and computing the cumulative sum of unique scanning sessions belonging to this subject.
# IMPORTANT: This assumes that the zipped dicoms are ordered temporally within subjects.
# TODO: make this more robust by using timestamps pulled from dicoms to assign session
# numbers.
df2['session'] = df2.groupby('subject').cumcount() + 1
df2['session'] = ['{0:0>2}'.format(i) for i in df2['session']]
df2.sort_values(by=['session', 'participant_id'], inplace=True)

# Saves the two files sessions.tsv and participants.tsv to the BIDS directory.
# participants.tsv contains a single row for each participant in the study,
# whereas sessions.tsv contains a single row for each unique scan in the study (i.e.,
# if patients have multiple scans, they will have multiple lines in sessions.tsv, but only
# one line in participants.tsv)
df2.to_csv(config['PATH']['bids_data'] + '/sessions.tsv', index=False, sep='\t')
df2[['participant_id', 'subject']].drop_duplicates().to_csv(config['PATH']['bids_data'] + '/participants.tsv', index=False, sep='\t')

# Lastly we use the sessions.tsv to pull the relevant information for participant,
# session, and dicom image location which we will pass to dicom2bids.
sessions = pd.read_csv(config['PATH']['bids_data'] + '/sessions.tsv', sep='\t',
                       dtype={'participant_id': object, 'session': object})

anat_dirs = glob.glob(config['PATH']['zipped_dicoms'] + '/*/*' + config['images']['T1']['regex'] + '*/', recursive=False)
T2w_dirs = glob.glob(config['PATH']['zipped_dicoms'] + '/*/*' + config['images']['T2']['regex'] + '*/', recursive=False)
func_dirs = glob.glob(config['PATH']['zipped_dicoms'] + '/*/*' + config['images']['func']['regex'] + '*/', recursive=False)
anat_sessions = [i.split('/')[5] for i in anat_dirs]
anat_scans = [i.split('/')[6] for i in anat_dirs]
T2w_sessions = [i.split('/')[5] for i in T2w_dirs]
T2w_scans = [i.split('/')[6] for i in T2w_dirs]
func_sessions = [i.split('/')[5] for i in func_dirs]
func_scans = [i.split('/')[6] for i in func_dirs]

anat_df = pd.DataFrame({'sessions': anat_sessions, 'anat': anat_scans})
T2w_df = pd.DataFrame({'sessions': T2w_sessions, 'T2w': T2w_scans})

func_df = pd.DataFrame({'sessions': func_sessions, 'func': func_scans})
func_df['scan_num'] = func_df.groupby('sessions').cumcount() + 1

first_anat_df = anat_df.groupby('sessions').nth(0).reset_index()
first_T2w_df = T2w_df.groupby('sessions').nth(0).reset_index()
first_third_func_df = func_df.groupby('sessions').nth(config['images']['func']['series_num']).reset_index()
first_third_func_df = first_third_func_df.pivot(index='sessions', columns='scan_num', values='func').reset_index()


merged = first_anat_df.merge(first_T2w_df, on = 'sessions', how='outer')
merged = merged.merge(first_third_func_df, on = 'sessions', how='outer')
merged.columns = ['scan', 'T1w', 'T2w','func_1', 'func_2']


sessions = sessions.merge(merged, on = 'scan')
sessions = sessions.replace(np.nan, '', regex=True)
sessions['T1w_series'] = [i[0:3] for i in sessions['T1w']]
sessions['T2w_series'] = [i[0:3] for i in sessions['T2w']]
sessions['func_1_series'] = [i[0:3] for i in sessions['func_1']]
sessions['func_2_series'] = [i[0:3] for i in sessions['func_2']]

## This chunk converts to bids proper, with sidecars
scan = sessions['scan']
dirs = [config['PATH']['zipped_dicoms'] + '/' + i + '/' for i in scan]
sub = sessions['subject']
sess = [re.sub('\D', '', i) for i in sessions['session']]
n = len(dirs)
sess_i = iter(sess)
dirs_i = iter(dirs)
sub_i = iter(sub)
# Converts the bids congiguration json to an absolute path. This is required
# for dcm2bids to run properly
bids_json = abspath(config_json)
bids_output = config['PATH']['bids_data']

# For each subject in our unzipped dicoms, converts their data
# to bids format. 
for i in range(0, n):
    a_iter = dirs_i.__next__()
    b_iter = sub_i.__next__()
    c_iter = sess_i.__next__()
    cmd = "dcm2bids -d %s -p %s -s %s -o %s -c %s" % (a_iter, b_iter, c_iter, bids_output, bids_json)
    os.system(cmd)

## This chonk does 'manual' bids, unpacking with freesurfer. Some analyses I did required
## freesurfer unpacking for some reason, but I don't know why. If you use
## dcm2bids above, the reorient.py script appears to reorient incorrectly

# iterate over all subjects and upack to bids
# for index, row in sessions.iterrows():
    
#     out_directory = config['PATH']['bids_data'] + '/' + MakeBidsPath(str(row['subject']), row['session'])
    
#     if not os.path.exists(out_directory):
#         os.makedirs(out_directory)
           
#         # nipype dosent appear to have dt_recon from freesurfer yet
#         #unpack_b01 = dt_recon()
#         #unpack_b01.inputs.source_dir = config['PATH']['zipped_dicoms'] + '/' + row['scan'] + '/' + row['func_2']
#         #unpack_b01.inputs.output_dir = out_directory
#         #unpack_b01.inputs.run_info = (int(row['func_2_series']), 'func', 'nii', MakeBidsFuncFile(row['subject'], row['session'], 'rest', '02'))
#         #unpack_b01.inputs.dir_structure = 'generic'
        
#         try:
#             unpack_anat = UnpackSDICOMDir()
#             unpack_anat.inputs.source_dir = config['PATH']['zipped_dicoms'] + '/' + row['scan'] + '/' + row['T1w']
#             unpack_anat.inputs.output_dir = out_directory
#             unpack_anat.inputs.run_info = (int(row['T1w_series']), 'anat', 'nii', MakeBidsT1wFile(str(row['subject']), row['session'], '01'))
#             unpack_anat.inputs.dir_structure = 'generic'
#             unpack_anat.run()
#         except:
#             pass
#         try:
#             unpack_T2w = UnpackSDICOMDir()
#             unpack_T2w.inputs.source_dir = config['PATH']['zipped_dicoms'] + '/' + row['scan'] + '/' + row['T2w']
#             unpack_T2w.inputs.output_dir = out_directory
#             unpack_T2w.inputs.run_info = (int(row['T2w_series']), 'anat', 'nii', MakeBidsT2wFile(str(row['subject']), row['session'], '01'))
#             unpack_T2w.inputs.dir_structure = 'generic'
#             unpack_T2w.run()
#         except:
#             pass
#         try:
#             unpack_func_1 = UnpackSDICOMDir()
#             unpack_func_1.inputs.source_dir = config['PATH']['zipped_dicoms'] + '/' + row['scan'] + '/' + row['func_1']
#             unpack_func_1.inputs.output_dir = out_directory
#             unpack_func_1.inputs.run_info = (int(row['func_1_series']), 'func', 'nii', MakeBidsFuncFile(str(row['subject']), row['session'], 'rest', '01'))
#             unpack_func_1.inputs.dir_structure = 'generic'
#             unpack_func_1.run()
#         except:
#             pass
#         try:
#             unpack_func_2 = UnpackSDICOMDir()
#             unpack_func_2.inputs.source_dir = config['PATH']['zipped_dicoms'] + '/' + row['scan'] + '/' + row['func_2']
#             unpack_func_2.inputs.output_dir = out_directory
#             unpack_func_2.inputs.run_info = (int(row['func_2_series']), 'func', 'nii', MakeBidsFuncFile(str(row['subject']), row['session'], 'rest', '02'))
#             unpack_func_2.inputs.dir_structure = 'generic'
#             unpack_func_2.run()
#         except:
#             pass

