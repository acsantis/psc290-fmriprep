#!/bin/bash

# Script for preparing EBM's macaque dicoms (1 anat + 2 rest)
# Steps include: unzip, unpack, correct orientation, skullstrip functional images; next step would be TOPUP and then spm script/fcfast
# Usage: 
# 1. Edit subject list file /autofs/cluster/iaslab/EBMK99/dicom/subj.lst; REMEMBER TO ONLY PUT SUBJECTS THAT YOU NEED TO UNPACK - AVOID OVERWRITE
# FORMAT OF FILE SHOULD BE, IN EACH LINE:
# SUBJID    		MPRAGE# 	1st RESTING#	2nd RESTING#
# 
# for example:
# 
# ds_2015-04-07_07-53	5		19		20
# ds_2015-03-03_07-48 	7		21		22
#
# 2. Enter in any bash window this command: sh /autofs/cluster/iaslab/EBMK99/scripts/prep.sh /autofs/cluster/iaslab/EBMK99/dicom/subj.lst

# If dicoms have been consistently named & MPRAGE# <10 & RESTING# >10, then nothing in the following script should be changed.

datadir=/mnt/c/Users/Anthony\ Santistevan/data/imaging/EBMK99
codedir=/mnt/c/Users/Anthony\ Santistevan/code/imaging/preprocessing

cd "$datadir"

cat "$1" | while read -r line; do
        subj=$(echo $line | awk '{print $1}')
        anat=$(echo $line | awk '{print $2}')
        bold1=$(echo $line | awk '{print $3}')
        bold2=$(echo $line | awk '{print $4}')


	# unzip original dicoms (should be done first using unzip.sh)
	unzip "$datadir"/dicom/${subj}.zip -d "$datadir"/dicom/
	
	dcmdir="$datadir"/dicom/${subj}	
	subdir="$datadir"/${subj}

	# unpack two resting state scans (run 019 and 020)
	unpacksdcmdir -src "$dcmdir"/0${bold1}_contrast_MB1G2_F__H -targ "$subdir" -fsfast -run ${bold1} bold nii ${subj}_bld0${bold1}_rest.nii
	unpacksdcmdir -src "$dcmdir"/0${bold2}_contrast_MB1G2_H__F -targ "$subdir" -fsfast -run ${bold2} bold nii ${subj}_bld0${bold2}_rest.nii
	# unpack one anatomical scan (run 005)
	if (("$anat"<10))
	then
		unpacksdcmdir -src "$dcmdir"/00${anat}_Sag_T1__256x256_0_3iso-from-interp_ -targ "$subdir" -fsfast -run ${anat} anat nii ${subj}_mpr00${anat}.nii
	else
		unpacksdcmdir -src "$dcmdir"/0${anat}_Sag_T1__256x256_0_3iso-from-interp_ -targ "$subdir" -fsfast -run ${anat} anat nii ${subj}_mpr0${anat}.nii
	fi

	# correct orientation 
	bolddir1="$subdir"/bold/0${bold1}
	bolddir2="$subdir"/bold/0${bold2}

	if (("$anat"<10))
	then
		anatdir="$subdir"/anat/00${anat}
	else
		anatdir="$subdir"/anat/0${anat}
	fi

	fslswapdim "$bolddir1"/${subj}_bld0${bold1}_rest.nii -x -y z "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz
	fslorient -deleteorient "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz
	fslorient -setqformcode 1 "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz
	mri_convert "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz -ic 0 0 0 "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz

	fslswapdim "$bolddir2"/${subj}_bld0${bold2}_rest.nii -x -y z "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz
	fslorient -deleteorient "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz
	fslorient -setqformcode 1 "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz
	mri_convert "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz -ic 0 0 0 "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz

	if (("$anat"<10))
	then
		fslswapdim "$anatdir"/${subj}_mpr00${anat}.nii z -y x "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz
		fslswapdim "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz -x y z "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz
		fslorient -deleteorient "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz
		fslorient -setqformcode 1 "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz
		mri_convert "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz -ic 0 0 0 "$anatdir"/${subj}_mpr00${anat}_reorient.nii.gz
	else
		fslswapdim "$anatdir"/${subj}_mpr0${anat}.nii z -y x "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz
		fslswapdim "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz -x y z "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz
		fslorient -deleteorient "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz
		fslorient -setqformcode 1 "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz
		mri_convert "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz -ic 0 0 0 "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz
	fi

	# BET functional image (-f threshold can be adjusted to change amount of skullstripping; recommendation: run all subjects with -f .1 and adjust as necessary by rerunning the following commands with different -f values, higher -f value = more aggressive skullstripping) THIS IS NOT NECESSARY IF RUN IN COMBINATION WITH DANTE'S PREPROCESSING SCRIPT
	#bet ${bolddir1}/${subj}_bld0${bold1}_rest_reorient.nii.gz ${bolddir1}/${subj}_bld0${bold1}_rest_reorient_bet.nii.gz -F -f .1
	#bet ${bolddir2}/${subj}_bld0${bold2}_rest_reorient.nii.gz ${bolddir2}/${subj}_bld0${bold2}_rest_reorient_bet.nii.gz -F -f .1
	#bet "$anatdir"/${subj}_mpr0${anat}_reorient.nii.gz "$anatdir"/${subj}_mpr0${anat}_reorient_bet.nii.gz -f .1

	# TOPUP and merge both functional runs together
	fslmaths "$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz -Tmean "$bolddir1"/${subj}_bld0${bold1}_rest_reorient_mean.nii.gz
	fslmaths "$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz -Tmean "$bolddir2"/${subj}_bld0${bold2}_rest_reorient_mean.nii.gz

	fslmerge -t "$subdir"/bold/BOLD_mean_merged.nii.gz "$bolddir1"/${subj}_bld0${bold1}_rest_reorient_mean.nii.gz "$bolddir2"/${subj}_bld0${bold2}_rest_reorient_mean.nii.gz

	topup --imain="$subdir"/bold/BOLD_mean_merged.nii.gz --datain="$codedir"/topup_datain.txt --config=b02b0.cnf --out="$subdir"/bold/topup_output

	applytopup --imain="$bolddir1"/${subj}_bld0${bold1}_rest_reorient.nii.gz,"$bolddir2"/${subj}_bld0${bold2}_rest_reorient.nii.gz --datain="$codedir"/topup_datain.txt --topup="$subdir"/bold/topup_output --inindex=1,2 --out="$subdir"/bold/corrected_BOLD_merged
	

done
