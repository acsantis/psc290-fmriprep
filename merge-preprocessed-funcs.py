#!/usr/bin/env python
# coding: utf-8

# In[1]:


# get the Node, Workflow, SelectFiles, and MapNode objects from the NiPype package 
from nipype import Node, Workflow, SelectFiles, DataSink, MapNode
from nipype.interfaces import fsl

# import other general packages, such as nibabel 
from IPython.display import SVG, Image
import nibabel as nb
import numpy as np
import bids.layout

# ignore ugly wargnings because ugly
import warnings
warnings.filterwarnings('ignore')

# yaml for reading in configs
import yaml

# loads up configuration files
config_yaml = '/home/neuro/code/config-EBM05.yml'
config = yaml.safe_load(open(config_yaml))
from nipype.interfaces.fsl import SwapDimensions
from pypelinefuncs.fslorient import FslOrient
from nipype.interfaces.freesurfer import MRIConvert
from nipype.algorithms.misc import Gunzip
from pypelinefuncs.extractmetadata import GetFieldFromSidecar
from nipype.interfaces.spm import SliceTiming, NewSegment
import nipype.interfaces.spm as spm
import nibabel as nib
# import workflow
from pypelinefuncs.workflows import create_reorient_flow
from pypelinefuncs.workflows import create_topup_flow

import glob
import re
import pandas as pd
import numpy as np

from nipype.interfaces import fsl
from shutil import copyfile
import os as os


# In[4]:


topup1 = glob.glob("/home/neuro/output/EBM05-preprocessed-NMT-run01/*/*/preprocessed_output/wemfunctional4D.nii", recursive=True)
topup2 = glob.glob("/home/neuro/output/EBM05-preprocessed-NMT-run02/*/*/preprocessed_output/wemfunctional4D.nii", recursive=True)


# In[5]:


subs1 = [re.search("sub-[0-9][0-9]", i).group(0) for i in topup1]
subs2 = [re.search("sub-[0-9][0-9]", i).group(0) for i in topup2]


# In[6]:


df1 = pd.DataFrame({"subject" : subs1, "file" : topup1})
df2 = pd.DataFrame({"subject" : subs2, "file" : topup2})


# In[7]:


merged = df1.merge(df2, left_on = 'subject', right_on = 'subject', how = 'outer')


# In[9]:


for index, row in merged.iterrows():
    try:
        os.mkdir('/home/neuro/output/EBM05-preprocessed-NMT-merged/' + row['subject'])
    except:
        print('/home/neuro/output/EBM05-preprocessed-NMT-merged/' + row['subject'] + ' already exists!')
    try:
        merge = fsl.Merge()
        merge.inputs.dimension = 't'
        merge.inputs.in_files = [row['file_x'], row['file_y']]
        merge.inputs.merged_file = '/home/neuro/output/EBM05-preprocessed-NMT-merged/' + row['subject'] + '/wemfunctional4D_merged.nii'
        merge.inputs.output_type = 'NIFTI'
        merge.run()
    except:
        try:
            copyfile(row['file_x'], '/home/neuro/output/EBM05-preprocessed-NMT-merged/' + row['subject'] + '/wemfunctional4D.nii')
        except:
            copyfile(row['file_y'], '/home/neuro/output/EBM05-preprocessed-NMT-merged/' + row['subject'] + '/wemfunctional4D.nii')


# In[ ]:




