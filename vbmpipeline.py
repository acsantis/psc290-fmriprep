#!/usr/bin/env python
# coding: utf-8

# get the Node, Workflow, SelectFiles, and MapNode objects from the NiPype
# package
import glob
import pandas as pd
import yaml
import os
import sys

from datetime import datetime
from shutil import copyfile
from pypelinefuncs.workflows import vbm_template_flow
from pypelinefuncs.fslorient import FslOrient
from nipype.interfaces.freesurfer import MRIConvert
from nipype.interfaces.fsl.maths import MathsCommand
from nipype.interfaces.fsl import SwapDimensions
from nipype.interfaces import fsl
from nipype.interfaces import spm
from nipype.interfaces import utility as niu
from nipype.interfaces.utility import Function
from nipype.algorithms.misc import Gunzip
from nipype import Node, Workflow, SelectFiles, DataSink, MapNode

# import other general packages, such as nibabel
from IPython.display import SVG, Image
import nibabel as nb
import numpy as np
import bids.layout

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
from utils import plot_nii

def vbm_1_preproc(path_ref, dir_subj, dir_out, subjects, sessions, n_procs,
                  config, path_sklstrp, dir_plot=None, subdir_plot='/auto'):
    """ Preprocesses strucutral data for VBM analysis:
        1) brain extraction
        2) tissue segmentation into CSF, GM, WM
        3) affine registration to atlas

    Arguments:
        - path_ref: str
            Path to the reference .nii for FLIRT.
        - dir_subj: str
            Path to the directory containing the subject data.
        - dir_out: str
            Path to the directory where the outputs will be generated.
        - subjects: str
            List of subjects whose data will be processed.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html

    Raises:
        - ValueError: if @subdir_plot is not string.

    """

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    gm_images = {'GM': '_session_id_{session}_subject_id_{subject}/sub-{subject}_ses-{session}_run-01_T1w_reoriented.nii'}

    sf = Node(SelectFiles(gm_images,
                          base_directory=dir_subj,
                          sort_filelist=True),
              name='selectfiles')

    def _get_skullstripped_file(in_file, path_sklstrp):
        """ Inner function for getting corresponding skullstripped file
        Args:
            - in_file: str
                Path to the original nii file
            - path_sklstrp: str
                Path to the directory containing the skullstripped files.
        Rets:
            - path_nii: str
                Path to the original nii file
            - path_mask:
                Path to the corresponding skullstripped file.
        """
        fn = in_file.split('/')[-1]
        fn = fn.replace('reoriented', 'extraction')
        path_mask = path_sklstrp + fn + '.gz'
        return in_file, path_mask

    # Interface function
    get_skullstripped_file = Function(input_names=['in_file', 'path_sklstrp'],
                                      output_names=['path_nii', 'path_mask'],
                                      function=_get_skullstripped_file)

    get_skullstripped = Node(get_skullstripped_file, name = 'get_skullstripped')
    get_skullstripped.inputs.path_sklstrp = path_sklstrp
    apply_mask = Node(fsl.maths.ApplyMask(), name='apply_mask')

    flirt_t1_f99 = Node(fsl.FLIRT(), name = 'flirt_t1_f99')
    flirt_t1_f99.inputs.reference = config['PATH']['template']['T1w']

    gunzip = Node(Gunzip(), name = 'gunzip')

    seg = Node(spm.Segment(), name = 'seg')
    seg.inputs.gm_output_type = [False, False, True]
    seg.inputs.wm_output_type = [False, False, True]
    seg.inputs.csf_output_type = [False, False, True]
    seg.inputs.save_bias_corrected = True
    seg.inputs.clean_masks = 'no'
    seg.inputs.tissue_prob_maps= [config['PATH']['template']['GM'], config['PATH']['template']['WM'], config['PATH']['template']['CSF']]
    seg.inputs.gaussians_per_class = [2, 2, 2, 4]
    seg.inputs.affine_regularization = 'subj'
    seg.inputs.warping_regularization = 1
    seg.inputs.bias_regularization = 1e-5
    seg.inputs.bias_fwhm = 30
    seg.inputs.sampling_distance = 3

    flirt = Node(fsl.FLIRT(), name='register_GM_to_atlas')
    flirt.inputs.reference = path_ref
    flirt.inputs.out_file = 'gm_volume_image.nii.gz'

    vbm_1_preproc = Workflow(name='vbm_1_preproc', base_dir=dir_out)
    sf.iterables = [('subject', subjects), ('session', sessions)]

    vbm_1_preproc.connect([
        (sf, get_skullstripped, [('GM', 'in_file')]),
        (get_skullstripped, apply_mask, [('path_nii', 'in_file'),
                                         ('path_mask', 'mask_file')]),
        (apply_mask, flirt_t1_f99, [('out_file', 'in_file')]),
        (flirt_t1_f99, gunzip, [('out_file', 'in_file')]),
        (gunzip, seg, [('out_file', 'data')]),
        (seg, flirt, [('native_gm_image', 'in_file')])
    ])


    # Visualize the workflow
    vbm_1_preproc.write_graph(graph2use='colored',
                         format='png',
                         simple_form=False)

    Image(filename=dir_out + 'vbm_1_preproc/graph.png', width=250)
    vbm_1_preproc.run('MultiProc', plugin_args={'n_procs': n_procs})

    pattern = dir_out + 'vbm_1_preproc/_session_*_subject_*/register_GM_to_atlas/gm_volume_image.nii.gz'
    plot_n_save(dir_plot, subdir_plot, pattern, bg_img=config['PATH']['template']['T1w'])


def create_first_study_template(dir_base, config, dir_plot=None, subdir_plot='/auto'):
    """ Create first study template.
    Create first study template by averaging, mirroring across x-axis, and
      averaging the original and mirrored images.

    Arguments:
        - dir_base: str
            The base directory for VBM template. This should also be the
            directory where FLIRT generated its output.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html
    """
    # get all of the flirted images
    pattern = dir_base + 'vbm_1_preproc/_session_*_subject_*/register_GM_to_atlas/gm_volume_image.nii.gz'
    GM_flirt = glob.glob(pattern)
    GM_flirt.sort()

    vbm_template = vbm_template_flow(name='vbm_template_1',
                                     in_files=GM_flirt,
                                     base_dir=dir_base)

    # Visualize the workflow
    vbm_template.write_graph(graph2use='colored',
                             format='png',
                             simple_form=False)

    Image(filename=dir_base + 'vbm_template_1/graph.png', width=250)
    vbm_template.run()

    pattern = dir_base + 'vbm_template_1/*/*.nii.gz'
    plot_n_save(dir_plot, subdir_plot, pattern, bg_img=config['PATH']['template']['T1w'])


def nonlinear_register_first(dir_ref, dir_subj, path_fnirt_config, config, subjects, sessions,
                             n_procs, dir_plot=None, subdir_plot='/auto'):
    """ Nonlinearly register each GM segmentation to the first template.

    Arguments:
        - dir_ref: str
            Directory that contains the VBM templates and FLIRT outputs which
            will be passed into FNIRT.
        - dir_subj: str
            Directory that contains the subject data.
        - path_fnirt_config: str
            Path to the FNIRT configuration file.
        - subjects: list
            List of subjects whose data to be processed.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html

    """
    fnirt_wf = Workflow(name='fnirt_initial_template', base_dir=dir_ref)

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    gm_images = {'GM': dir_ref +
                 'vbm_1_preproc/_session_{session}_subject_{subject}/register_GM_to_atlas/' + \
                 'gm_volume_image.nii.gz'}

    fnirt_sf = Node(SelectFiles(gm_images,
                                base_directory=dir_subj,
                                sort_filelist=True),
                    name='fnirt_selectfiles')

    fnirt_sf.iterables = [('subject', subjects), ('session', sessions)]

    fnirt = Node(fsl.FNIRT(), name='fnirt')

    fnirt.inputs.ref_file = dir_ref + \
        'vbm_template_1/template/vbm_template_1.nii.gz'
    fnirt.inputs.config_file = path_fnirt_config

    fnirt_wf.connect([
        (fnirt_sf, fnirt, [('GM', 'in_file')])])

    fnirt_wf.run('MultiProc', plugin_args={'n_procs': n_procs})

    pattern = dir_ref + \
        'fnirt_initial_template/_session_*_subject_*/fnirt/' + \
        'gm_volume_image_warped.nii.gz'
    plot_n_save(dir_plot, subdir_plot, pattern,
        bg_img=dir_ref + \
        'vbm_template_1/template/vbm_template_1.nii.gz')

def create_final_study_template(dir_out, config, dir_plot=None, subdir_plot='/auto'):
    """ Create the final study template.
    Create final study template by averaging nonlinearly registered GM images,
        mirroring across x-axis, and averaging with the mirrored image.

    Arguments:
        - dir_out: str
            Path to the directory that contains FNIRT outputs and VBM templates.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html
    """
    # get all of the flirted images
    GM_fnirt = glob.glob(
        dir_out +
        'fnirt_initial_template/_session_*_subject_*/fnirt/gm_volume_image_warped.nii.gz')
    GM_fnirt.sort()
    GM_fnirt.append(
        dir_out +
        'vbm_template_1/template/vbm_template_1.nii.gz')

    vbm_final_template = vbm_template_flow(
        name='vbm_final_template',
        in_files=GM_fnirt,
        base_dir=dir_out)

    # Visualize the workflow
    vbm_final_template.write_graph(graph2use='colored',
                                   format='png',
                                   simple_form=False)
    Image(filename=dir_out+ 'vbm_final_template/graph.png', width=250)
    vbm_final_template.run()


    pattern = dir_out + \
        'vbm_final_template/*/*.nii.gz'
    plot_n_save(dir_plot, subdir_plot, pattern, bg_img=config['PATH']['template']['T1w'])

def nonlinear_register_to_final(dir_out, path_fnirt_config, config, subjects, sessions, n_procs,
                                dir_plot=None, subdir_plot='/auto'):
    """ Nonlinearly register each GM segmentation to the final nonlinear
        template.
    Save the jacobians and modulate the GM images by the jacobians, and smooth
        them.
    Arguments:
        - dir_out: str
            Path to the directory that contains FNIRT outputs and VBM templates.
        - path_fnirt_config: str
            Path to the FNIRT configuration file.
        - subjects: list
            List of subjects whose data to be processed.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html
    """
    fnirt_wf_2 = Workflow(name='fnirt_final_template', base_dir=dir_out)
    fnirt_2 = Node(fsl.FNIRT(), name='fnirt2')
    fnirt_2.inputs.ref_file = dir_out + \
        'vbm_final_template/template/' + \
        'vbm_final_template.nii.gz'
    fnirt_2.inputs.jacobian_file = True
    fnirt_2.inputs.config_file = path_fnirt_config
    fnirt_2.inputs.refmask_file = dir_out + \
        'vbm_final_template/dil/' + \
        'vbm_final_template_dil_mask.nii.gz'

    multiply = Node(fsl.MultiImageMaths(), name='modulate')
    multiply.inputs.op_string = '-mul %s'

    smooth = Node(fsl.IsotropicSmooth(), name='smooth')
    smooth.inputs.fwhm = 3

    gm_images = {'GM': dir_out +
                 'vbm_1_preproc/_session_{session}_subject_{subject}/register_GM_to_atlas/gm_volume_image.nii.gz'}

    # create SelectFiles node
    # This node uses the new templates to find files. Below, we added a call to
    # an external .yml file, which automates selection of appropriate files
    fnirt_sf2 = Node(SelectFiles(gm_images,
                                 base_directory=dir_out,
                                 sort_filelist=True),
                     name='fnirt_selectfiles_2')
    fnirt_sf2.iterables = [('subject', subjects), ('session', sessions)]
    fnirt_wf_2.connect([
        (fnirt_sf2, fnirt_2, [('GM', 'in_file')]),
        (fnirt_2, multiply, [('warped_file', 'in_file')]),
        (fnirt_2, multiply, [('jacobian_file', 'operand_files')]),
        (multiply, smooth, [('out_file', 'in_file')])
    ])
    fnirt_wf_2.run('MultiProc', plugin_args={'n_procs': n_procs})

    pattern = dir_out + \
        'fnirt_final_template/_session_*_subject_*/*/*.nii.gz'
    plot_n_save(dir_plot, subdir_plot, pattern,
        bg_img = dir_out + \
        'vbm_final_template/template/' + \
        'vbm_final_template.nii.gz')

def plot_n_save(dir_plot, subdir_plot, pattern, bg_img):
    """ Wrapper function for plot_nii.save() that automatically determines path.

    Arguments:
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html
        - pattern: str
            UNIX wildcard pattern that matches all the nii.gz files to be
            plotted.
        - dir_plot: str, default None
            Path to the directory that contains the plots.
            If None, no plot will be generated. Othewise, plots will be
              generated under
              @dir_plot/@subdir_plot/*.html
        - subdir_plot: str, default '/auto'
            If '/auto', plots will be generated under
                @dir_plot/function_name/*.html
            Otherwise, plots will be generated under
                @dir_plot/@subdir_plot/*.html

    Raises:
        - ValueError: if @subdir_plot is not string.
    """
    if dir_plot:
        if subdir_plot == '/auto':
            func_name = sys._getframe().f_back.f_code.co_name
            dir_plot = os.path.join(dir_plot, func_name)
        elif type(subdir_plot).__name__ == 'str':
            dir_plot = os.path.join(dir_plot, subdir_plot)
        else:
            raise ValueError('@subdir_plot must be str')

        plot_nii.save(pattern, dir_plot, bg_img=bg_img)

def main():
    # rename error logging file for plotting if exists
    if os.path.exists('errors_plot_nii'):
        ts = hex(int((datetime.now()).timestamp()))[4:] # timestamp
        os.rename('errors_plot_nii', 'errors_plot_nii_' + ts)

    # loads up configuration files
    with open('/home/neuro/code/config-EBM05.yml') as f:
        config = yaml.safe_load(f)

    path = config['PATH']

    # Save the plots for the original nii
    plot_nii.save(
        path['data'] + '*/*.nii',
        path['plots'] + 'orig/',
        bg_img = None)

    vbm_1_preproc(
        path_ref = path['template']['GM_LR'],
        dir_subj = path['data'],
        dir_out = path['output'],
        subjects = config['subjects'],
        sessions = config['sessions'],
        config = config,
        path_sklstrp = path['skullstripped'],
        n_procs = config['n_procs'],
        dir_plot = path['plots']
        )

    create_first_study_template(
        dir_base = path['output'],
        dir_plot = path['plots'],
        config = config
        )

    nonlinear_register_first(
        dir_ref = path['output'],
        dir_subj = path['data'],
        path_fnirt_config = path['fnirt']['CNF'],
        config = config,
        subjects = config['subjects'],
        sessions = config['sessions'],
        n_procs = config['n_procs'],
        dir_plot = path['plots']
        )

    create_final_study_template(
        dir_out = path['output'],
        dir_plot = path['plots'],
        config = config)

    nonlinear_register_to_final(
        dir_out = path['output'],
        path_fnirt_config = path['fnirt']['CNF'],
        config = config,
        subjects = config['subjects'],
        sessions = config['sessions'],
        n_procs = config['n_procs'],
        dir_plot = path['plots']
        )


if __name__ == '__main__':
    main()
