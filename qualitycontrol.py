#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from nilearn import image as nli
from nilearn.plotting import plot_epi, plot_stat_map
import pylab as plt
import yaml
get_ipython().run_line_magic('matplotlib', 'inline')

config_yaml = '/home/neuro/code/config-pipeline.yml'
config = yaml.safe_load(open(config_yaml))

# The subject for which we would like to perform QA
sub = "01"
# The task we are using
task = "Cyberball"


# In[2]:


#Motion Correction and Artifact Detection
#How much the subject moved in the scanner and outliers in the functional images
output = config['output'] + '/' + config['preprocessed_output'] + '/' + '_subject_id_%s/' % (sub)

par = np.loadtxt(output + '/mcflirt/'
                 'asub-%s_task-%s_bold_roi_mcf.nii.gz.par' % (sub, task))
fig, axes = plt.subplots(2, 1, figsize=(15, 5))
axes[0].set_ylabel('rotation (radians)')
axes[0].plot(par[0:, :3])
axes[1].plot(par[0:, 3:])
axes[1].set_xlabel('time (TR)')
axes[1].set_ylabel('translation (mm)');


# In[12]:


#the tissue probability maps
anat = output + 'gunzip_anat/sub-%s_T1w.nii' % (sub)

plot_stat_map(
    output + 'segment/c1sub-%s_T1w.nii' % (sub),
    title='GM prob. map', 
    cmap=plt.cm.magma,
    threshold=0.5,
    bg_img=anat,
    display_mode='z',
    cut_coords=range(0, 125, 25),
    dim=-1)

plot_stat_map(
    output + 'segment/c2sub-%s_T1w.nii' % (sub),
    title='WM prob. map',
    cmap=plt.cm.magma,
    threshold=0.5,
    bg_img=anat,
    display_mode='z',
    cut_coords=range(0, 125, 25),
    dim=-1)

plot_stat_map(
    output + 'segment/c3sub-%s_T1w.nii' % (sub),
    title='CSF prob. map',
    cmap=plt.cm.magma,
    threshold=0.5,
    bg_img=anat,
    display_mode='z',
    cut_coords=range(0, 125, 25),
    dim=-1)

plot_stat_map(
    output + 'mask_GM/c1sub-%s_T1w_flirt_thresh.nii' % (sub),
    title='dilated GM Mask',
    cmap=plt.cm.magma,
    threshold=0.5,
    bg_img=anat,
    display_mode='z',
    cut_coords=range(0, 125, 25),
    dim=-1)


# In[20]:


#Functional Image transformation

plot_epi(output + 'mcflirt/asub-%s_task-%s_bold_roi_mcf.nii.gz_mean_reg.nii.gz' % (sub, task),
         title='Motion Corrected mean image',
         display_mode='z',
         cut_coords=range(0, 125, 25),
         cmap=plt.cm.viridis)

mean = nli.mean_img(output + 'applywarp/asub-%s_task-%s_bold_roi_mcf_flirt.nii' % (sub, task))

plot_epi(mean, title='Coregistred mean image',
         display_mode='z',
         cut_coords=range(0, 125, 25),
         cmap=plt.cm.viridis)


plot_epi(output + 'detrend/mean.nii.gz',
         title='Detrended, masked, and smoothed mean image',
         display_mode='z',
         cut_coords=range(0, 125, 25),
         cmap=plt.cm.viridis);

