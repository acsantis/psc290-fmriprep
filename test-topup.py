from nipype import Node, Workflow, SelectFiles, DataSink, MapNode, Function
from nipype.interfaces import fsl
from pypelinefuncs.workflows import create_reorient_flow
from pypelinefuncs.workflows import create_topup_flow
from nipype.interfaces.fsl import SwapDimensions, BET
from pypelinefuncs.fslorient import FslOrient
from nipype.interfaces.freesurfer import MRIConvert
from nipype.algorithms.misc import Gunzip
from pypelinefuncs.extractmetadata import GetFieldFromSidecar
from nipype.interfaces.spm import SliceTiming, NewSegment
import nipype.interfaces.spm as spm
import nibabel as nib
# import workflow
from pypelinefuncs.workflows import create_reorient_flow
from pypelinefuncs.workflows import create_topup_flow

topup_wf = Workflow(name='topup', base_dir='/home/neuro/output')

# Effective Echo Spacing (s) = 1/(BandwidthPerPixelPhaseEncode * MatrixSizePhase)
# Total readout time (FSL) = (number of echoes - 1) * echo spacing

# EBM05
# MatrixSizePhase = 160
# BandwidthPerPixelPhaseEncode = 6.649
# n echos = 
# echo spacing = 0.94 ms
# phase encoding steps = 70
# aquisition time = 70 * 0.94 = 65.8 ms = 0.0658 s

# EST06
# MatrixSizePhase = 
# BandwidthPerPixelPhaseEncode =
# n echos = 
# echo spacing = 0.94 ms
# phase encoding steps = 50
# aquisition time = 50 * 0.94 = 65.8 ms = 0.0658 s

topup = create_topup_flow()
# 28394
topup.inputs.inputnode.in_file1 = "/home/neuro/output/reoriented/sub-27596_ses-01_task-rest_run-01_bold_newdims_out.nii"
topup.inputs.inputnode.in_file2 = "/home/neuro/output/reoriented/sub-27596_ses-01_task-rest_run-02_bold_newdims_out.nii"

topup_wf.add_nodes([(topup)])

topup_wf.run()