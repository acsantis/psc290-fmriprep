from nipype import Node, Workflow, Function
from nipype.interfaces import utility as niu
#from nipype.interfaces.fsl import SwapDimensions
#from nipype.interfaces.fsl.maths import MeanImage
#from nipype.interfaces.fsl.epi import ApplyTOPUP, TOPUP
#from nipype.interfaces.fsl import Merge
from nipype.interfaces import fsl
from pypelinefuncs.fslorient import FslOrient
#from nipype.interfaces.freesurfer import MRIConvert
from nipype.interfaces import freesurfer

def mriconvert_flow(in_orientation, out_orientation, name='reorient', out_format='stl'):

    mriconvert = Workflow(name=name)

    inputnode = Node(niu.IdentityInterface(fields=['in_file']), name='inputnode')
    
    # Delete old orientation
    orient = Node(FslOrient(), name = 'deleteorient')
    orient.inputs.main_option = 'deleteorient'
    # Overwrite new orientation
    setqformcode = Node(FslOrient(), name = 'setqformcode')
    setqformcode.inputs.main_option = 'setqformcode 1'
    # convert to sphinx
    sphinx = Node(freesurfer.MRIConvert(in_center = [0, 0, 0]), name = 'sphinx')
    sphinx.inputs.out_type = 'niigz'
    sphinx.inputs.sphinx = True

    convert = Node(freesurfer.MRIConvert(in_center = [0, 0, 0]), name = 'mriconvert')
    convert.inputs.out_type = 'niigz'
    convert.inputs.in_orientation = in_orientation
    convert.inputs.out_orientation = out_orientation
    convert.inputs.out_type = 'niigz'

    mriconvert.connect([
        (inputnode, orient, [('in_file', 'in_file')]),
        (orient, setqformcode, [('out_file', 'in_file')]),
        (setqformcode, sphinx, [('out_file', 'in_file')]),
        (sphinx, convert, [('out_file', 'in_file')])
        ])
    """
    Setup an outputnode that defines relevant inputs of the workflow.
    """

    outputnode = Node(niu.IdentityInterface(fields=["reoriented"]), name="outputnode")
                    
    mriconvert.connect([(convert, outputnode, [("out_file", "reoriented")])])
    
    return mriconvert

def create_reorient_flow(name='reorient', new_dims=('x', 'y', 'z'), out_format='stl'):

    reorient = Workflow(name=name)

    inputnode = Node(niu.IdentityInterface(fields=['in_file']), name='inputnode')
    # swap dimensions
    swap_dim = Node(fsl.SwapDimensions(new_dims = new_dims), name = 'swap_dim')
    # Delete old orientation
    orient = Node(FslOrient(), name = 'deleteorient')
    orient.inputs.main_option = 'deleteorient'
    # Overwrite new orientation
    setqformcode = Node(FslOrient(), name = 'setqformcode')
    setqformcode.inputs.main_option = 'setqformcode 1'
    # I'm not sure what this does but it was in the old code...
    mriconvert = Node(freesurfer.MRIConvert(in_center = [0, 0, 0]), name = 'mriconvert')
    mriconvert.inputs.out_type = 'nii'
    
    reorient.connect([
        (inputnode, swap_dim, [('in_file', 'in_file')]),
        (swap_dim, orient, [('out_file', 'in_file')]),
        (orient, setqformcode, [('out_file', 'in_file')]),
        (setqformcode, mriconvert, [('out_file', 'in_file')]),
        ])
    """
    Setup an outputnode that defines relevant inputs of the workflow.
    """

    outputnode = Node(niu.IdentityInterface(fields=["reoriented"]), name="outputnode")
                    
    reorient.connect([(mriconvert, outputnode, [("out_file", "reoriented")])])
    
    return reorient

def create_topup_flow(name='topup_flow', out_format='stl', encoding_file='/home/neuro/code/topup_datain.txt'):
    
    def concatenate_files(in_file1, in_file2):
        fout = [in_file1, in_file2]
        return fout

    topup_flow = Workflow(name=name)

    inputnode = Node(niu.IdentityInterface(fields=['in_file1', 'in_file2']), name='inputnode')
    
    mean_img_1 = Node(fsl.maths.MeanImage(), name='mean_img_1')
    mean_img_1.inputs.dimension = 'T'
    
    mean_img_2 = Node(fsl.maths.MeanImage(), name='mean_img_2')
    mean_img_2.inputs.dimension = 'T'
    
    concatenate = Node(Function(input_names=["in_file1", "in_file2"],
                                output_names=["out_files"],
                                function=concatenate_files),
                       name="concat_files")
    
    concatenate_means = Node(Function(input_names=["in_file1", "in_file2"],
                                      output_names=["out_files"],
                                      function=concatenate_files),
                             name="concat_mean_files")

    concatenate_corrected = Node(Function(input_names=["in_file1", "in_file2"],
                                      output_names=["out_files"],
                                      function=concatenate_files),
                             name="concat_corrected_files")
        
    merged_means = Node(fsl.Merge(), name = 'merged_means')
    merged_means.inputs.dimension = 't'
    
    merged_corrected = Node(fsl.Merge(), name = 'merged_corrected')
    merged_corrected.inputs.dimension = 't'
    
    topup = Node(fsl.epi.TOPUP(), name = 'topup')
    topup.inputs.encoding_file = encoding_file
    
    #applytopup = Node(fsl.epi.ApplyTOPUP(), name = 'applytopup')
    #applytopup.inputs.encoding_file = '/home/neuro/code/topup_datain.txt'
    #applytopup.inputs.output_type = 'NIFTI'
    
    applytopup1 = Node(fsl.epi.ApplyTOPUP(), name = 'applytopup1')
    applytopup1.inputs.encoding_file = encoding_file
    applytopup1.inputs.in_index=[1]
    applytopup1.inputs.method='jac'
    applytopup1.inputs.output_type = 'NIFTI'
    
    applytopup2 = Node(fsl.epi.ApplyTOPUP(), name = 'applytopup2')
    applytopup2.inputs.encoding_file = encoding_file
    applytopup2.inputs.in_index=[2]
    applytopup2.inputs.method='jac'
    applytopup2.inputs.output_type = 'NIFTI'
    
    topup_flow.connect([
        (inputnode, mean_img_1, [('in_file1', 'in_file')]),
        (inputnode, mean_img_2, [('in_file2', 'in_file')]),
        (inputnode, concatenate, [('in_file1', 'in_file1')]),
        (inputnode, concatenate, [('in_file2', 'in_file2')]),
        (mean_img_1, concatenate_means, [('out_file', 'in_file1')]),
        (mean_img_2, concatenate_means, [('out_file', 'in_file2')]),
        (concatenate_means, merged_means, [('out_files', 'in_files')]),
        # uses temporal mean of each epi collected w/opposite blips to
        # estimate field
        (merged_means, topup, [('merged_file', 'in_file')]),
        # original topup
        #(topup, applytopup, [('out_fieldcoef', 'in_topup_fieldcoef'),
        #                    ('out_movpar', 'in_topup_movpar')]),
        #(concatenate, applytopup, [('out_file', 'in_files')]),
        (inputnode, applytopup1, [('in_file1', 'in_files')]),
        (topup, applytopup1, [('out_fieldcoef', 'in_topup_fieldcoef'),
                            ('out_movpar', 'in_topup_movpar')]),
        (inputnode, applytopup2, [('in_file2', 'in_files')]),
        (topup, applytopup2, [('out_fieldcoef', 'in_topup_fieldcoef'),
                            ('out_movpar', 'in_topup_movpar')]),
        (applytopup1, concatenate_corrected, [("out_corrected", "in_file1")]),
        (applytopup2, concatenate_corrected, [("out_corrected", "in_file2")]),
        (concatenate_corrected, merged_corrected, [("out_files", "in_files")])
        ])

    outputnode1 = Node(niu.IdentityInterface(fields=["corrected"]), name="outputnode1")
    outputnode2 = Node(niu.IdentityInterface(fields=["corrected"]), name="outputnode2")
    outputnode3 = Node(niu.IdentityInterface(fields=["merged_corrected"]), name="outputnode3")
    
    topup_flow.connect([(applytopup1, outputnode1, [('out_corrected', 'corrected')]),
                        (applytopup2, outputnode2, [('out_corrected', 'corrected')]),
                        (merged_corrected, outputnode3, [('merged_file', 'merged_corrected')])])
    
        
    return topup_flow

def vbm_template_flow(name='vbm_template', in_files=None, base_dir=None, out_format='stl'):
    '''
        in_files: a list of grey matter segmented nii.gz files which are affine or nonlinearly registered
                  to a standard template or in the same space
        base_dir: directory on which to store outputs
    '''
    
    # create template flow
    vbm_template = Workflow(name=name, base_dir=base_dir)

    # merge affine registered GM images into one file
    merge = Node(fsl.Merge(), name='merge')
    merge.inputs.in_files = in_files
    merge.inputs.dimension = 't'

    # compute average
    mean = Node(fsl.ImageMaths(), name='mean')
    mean.inputs.op_string = '-Tmean'

    # flip average image
    mirror = Node(fsl.SwapDimensions(), name='mirror')
    mirror.inputs.new_dims = ('-x', 'y', 'z')

    # take average of mirrored and unmirroed average images
    template = Node(fsl.MultiImageMaths(), name='template')
    template.inputs.op_string = "-add %s -div 2"
    template.inputs.out_file = name + ".nii.gz"

    # Create a brainmask
    mask = Node(fsl.ImageMaths(), name='mask')
    mask.inputs.op_string = '-thr 0.3 -bin'

    # Dilate the brainmask
    dil = Node(fsl.ImageMaths(), name='dil')
    dil.inputs.op_string = '-dilM'
    dil.inputs.out_file = name + '_dil_mask.nii.gz'

    vbm_template.connect([
        (merge, mean, [('merged_file', 'in_file')]),
        (mean, mirror, [('out_file', 'in_file')]),
        (mean, template, [('out_file', 'in_file')]),
        (mirror, template, [('out_file', 'operand_files')]),
        (template, mask, [('out_file', 'in_file')]),
        (mask, dil, [('out_file', 'in_file')])
    ])
    
    """
    Setup an outputnode that defines relevant inputs of the workflow.
    """
    
    return vbm_template

def create_fslvbm_flow(name='reorient', new_dims=('x', 'y', 'z'), out_format='stl'):

    fslvbm = Workflow(name=name)

    inputnode = Node(niu.IdentityInterface(fields=['in_file']), name='inputnode')
    # swap dimensions
    swap_dim = Node(fsl.SwapDimensions(new_dims = new_dims), name = 'swap_dim')
    # Delete old orientation
    orient = Node(FslOrient(), name = 'deleteorient')
    orient.inputs.main_option = 'deleteorient'
    # Overwrite new orientation
    setqformcode = Node(FslOrient(), name = 'setqformcode')
    setqformcode.inputs.main_option = 'setqformcode 1'
    # I'm not sure what this does but it was in the old code...
    mriconvert = Node(freesurfer.MRIConvert(in_center = [0, 0, 0]), name = 'mriconvert')

    reorient.connect([
        (inputnode, swap_dim, [('in_file', 'in_file')]),
        (swap_dim, orient, [('out_file', 'in_file')]),
        (orient, setqformcode, [('out_file', 'in_file')]),
        (setqformcode, mriconvert, [('out_file', 'in_file')]),
        ])
    """
    Setup an outputnode that defines relevant inputs of the workflow.
    """

    outputnode = Node(niu.IdentityInterface(fields=["reoriented"]), name="outputnode")
                    
    reorient.connect([(mriconvert, outputnode, [("out_file", "reoriented")])])
    
    return reorient