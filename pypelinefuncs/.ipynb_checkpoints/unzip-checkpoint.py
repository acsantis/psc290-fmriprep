def UnzipSubDir(archive, criteria, output):
    """
        This function unzips only specified subdirectories from a compressed folder.
        
        Inputs:
            archive  - the name of the .zip file which to unzip
            criteria - regex which matches the directies which should be unzipped
        Outputs:
            Outputs a folder with the same same as 'archive.zip' with the 
            folders matching the criteria nested in the directory
    """
    import zipfile
    
    archive = zipfile.ZipFile(archive)
    for c in criteria: 
        for file in archive.namelist():
            if c in file:
                archive.extract(file, output)
