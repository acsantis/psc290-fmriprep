def GetFieldFromSidecar(bids_sidecar, field):
    """Function to extract metadata from fields in a sidecar .json file, so they don't have to be hard-coded"""
    # this function returns a list of information from the field specified, labeled "data," and can be used in further functions 
    # for example, the call below will return a list of slice times for a given volume of data
    # 'json' is a package available in nipype to efficiently read these text files 
    import json 
    # the 'open' argument points to a file "bids_sidecar," which is a header with information collected during each scan        
    # and 'f' (for field) is an argument specifying which row to pull information from, such as "SliceTiming"
    with open(bids_sidecar) as f:
        data = json.load(f)
    output = data[field]
    return(output)

# This function grabs the desired 'field' value from a given .dcm,
# and returns the falue of this field
def GetFieldFromDicom(f, field):
    import pydicom
    dcm = pydicom.dcmread(f)
    val = dcm.data_element(field)
    return(val.value)
