def get_gm(files):
    # Select GM segmentation file from segmentation output
    return files[0][0]

def get_wm(files):
    # To select the WM probability map that the NewSegment node created, we need some helper function
    # Select WM segmentation file from segmentation output
    return files[1][0]
