#!/usr/bin/env python
# coding: utf-8

# In[11]:


import numpy as np
from nilearn import image as nli
from nilearn import plotting
import pylab as plt
import yaml
from nilearn.image.image import mean_img

from nilearn import image

get_ipython().run_line_magic('matplotlib', 'inline')


# In[24]:


#subjects = ['01', '02', '03', '05', '06', '07', '08', '10', '11', '12', '13', '14', '15', '16', '17', '19', '20']
subjects = ['07']
sessions = ['01']

# good: '01', '02', '03', '04', '05', '06','07' ,'08', '10', '11', '12', '13', '14', '15', '16', '17', '19', '20'
# probs: '07'
for subj in subjects:
    for sess in sessions:
        display_anat = plotting.plot_anat('/home/neuro/data/EBM05-notopup/derivatives/reoriented/_session_id_{sess}_subject_id_{subj}/sub-{subj}_ses-{sess}_run-01_T1w_newdims_out_newdims_out.nii' .format(subj = subj, sess = sess), title= "T1w: subj: {subj} sess: {sess}" .format(subj = subj, sess = sess))
        #display_epi = plotting.plot_anat(mean_img('/home/neuro/data/EBM05-notopup/derivatives/func/_session_id_{sess}_subject_id_{subj}/sub-{subj}_ses-{sess}_task-rest_run-01_bold_newdims_out.nii' .format(subj = subj, sess = sess)), title = "EPI: subj: {subj} sess: {sess}" .format(subj = subj, sess = sess))


# In[ ]:




