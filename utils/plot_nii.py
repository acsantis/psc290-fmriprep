import os
import re
from glob import glob
from nilearn.plotting import view_img, plot_anat
from nilearn._utils.exceptions import DimensionError

class bc:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    BOLD = "\033[1m"

def save(pattern, dir_out, bg_img='/home/neuro/data/atlases/Macaque.112RM-F99space/112RM-F99_T1_hr.nii'):
    """ Generate the HTMLs of the input .nii.gz images.
    Arguments:
        - pattern: str
            UNIX wildcard pattern that describes the paths to the .nii.gz files
            that will be converted.
            E.g.: 'output/trial-*/subj-*/img.nii.gz'
        - dir_out: str
            Path to the directory where the HTMLs will be saved.
            Non-existing nested directory allowed.
    """
    paths = glob(pattern)
    # Convert to regex
    rplcmt = {'*': '(.*)',
              '?': '(.)',
              '[': '([',
              ']': '])'}
    for key in rplcmt:
         pattern = pattern.replace(key, rplcmt[key])

    # Get corresponding subject IDs
    subjs = ['-'.join(filter(None, re.search(pattern, p).groups()))
             for p in paths]

    os.makedirs(dir_out, exist_ok=True)

    for path, subj in zip(paths, subjs):
        try:
            #img = view_img(path, bg_img = bg_img, dim=-.5, cut_coords=(0, -0, 0), cmap='gray')
            #img.save_as_html(os.path.join(dir_out, subj))

            display = plot_anat(path, draw_cross=False, dim=-.5, cut_coords=(0, -0, 0))
            if bg_img:
                display.add_edges(bg_img)
            display.savefig(os.path.join(dir_out, subj) + '.png')
            display.close()

            msg = bc.OKGREEN + bc.BOLD + path + ' converted.' + bc.ENDC
            print(msg)
        except Exception as e:
            print()
            msg = (bc.BOLD + bc.WARNING + 'Error Caught\n' + bc.ENDC +
                   'While attempting to generate .html for the following file\n'
                   + bc.WARNING + '{}, \n' + bc.ENDC +
                   'the following error was caught: \n{}')
            print(msg.format(path, e))

            with open('errors_plot_nii', 'a+') as f:
                f.write(msg.format(path, e))
                f.write('\n\n')

            print()
