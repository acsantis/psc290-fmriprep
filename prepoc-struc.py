#!/usr/bin/env python
# coding: utf-8

# In[1]:


from bids.grabbids import BIDSLayout
import yaml as yaml
from nipype.pipeline import Node, MapNode, Workflow
from nipype.interfaces.utility import IdentityInterface, Function
import nipype.interfaces.spm as spm
import gzip
import os
import nibabel as nib
import numpy as np
import scipy.ndimage as ndimage


# In[2]:


# Load configuration file
config_yaml = '/home/neuro/code/imaging/pipeline/bids/params/config.yml'
config_json = '/home/neuro/code/imaging/pipeline/bids/params/config.json'
config = yaml.safe_load(open(config_yaml))


# In[84]:


layout = BIDSLayout(config['bids_output'])


# In[93]:


def get_T1w_niftis(subject_id, session_no, run_no, data_dir):
    # Remember that all the necesary imports need to be INSIDE the function for the Function Interface to work!
    from bids.grabbids import BIDSLayout
    
    layout = BIDSLayout(data_dir)
    
    T1w = layout.get(subject=subject_id, modality='anat', type='T1w', session=session_no,                      run=run_no, extensions=['nii', 'nii.gz'], return_type='file')
    
    return T1w

def clone_dir_structure(inputpath, outputpath):
    for dirpath, dirnames, filenames in os.walk(inputpath):
        structure = os.path.join(outputpath, dirpath[len(inputpath):])
        if not os.path.isdir(structure):
            os.mkdir(structure)

def gunzip(input, output):
    import shutil
    import gzip

    with gzip.open(input, 'rb') as f_in:
        with open(output, 'wb') as f_out:
            shutil.copyfileobj(f_in, f_out)

def fix_affine(image, output):
    # This function updates the affine matrix in the input image
    # and saves the output image
    a = np.asarray(image.shape)
    a = np.asmatrix(a)
    dim = a.transpose()
    replace = -image.affine[0:3, 0:3] * dim/2
    image.affine[0:3,3] = replace.transpose()
    nib.save(img=image,filename=output)
    return None


# In[86]:


# Create directory to store output
clone_dir_structure(config['bids_output'], config['derivatives'])


# In[87]:


T1w = get_T1w_niftis(subject_id='01', session_no='01', run_no='1', data_dir=config['bids_output'])
T1w_derivatives = [i.replace(config['bids_output'], config['derivatives']) for i in T1w]
T1w_derivatives = [i.replace('.nii.gz', '.nii') for i in T1w_derivatives]


# In[91]:


T1w = '/home/neuro/data/output/derivatives/sub-01_ses-01_run-01_T1w_reorient.nii.gz'


# In[8]:


#gunzip(T1w, T1w_derivatives[0])


# In[92]:


image = nib.load(T1w)


# In[94]:


fix_affine(image, T1w_derivatives[0])


# In[95]:


# Coregisters image to template space
coreg = spm.Coregister(cost_function='nmi',                        separation=[4, 2],                        tolerance=[0.02, 0.02, 0.02, 0.001, 0.001, 0.001, 0.01, 0.01, 0.01, 0.001, 0.001, 0.001],                        fwhm=[7, 7])
coreg.inputs.target = config['template']['T1w']
coreg.inputs.source = T1w_derivatives[0]
coreg.run() 


# In[171]:


seg = spm.Segment(gm_output_type = [False, False, True],                   wm_output_type = [False, False, True],                   csf_output_type = [False, False, True],                   save_bias_corrected = True,                   clean_masks = 'no',                   tissue_prob_maps=[config['template']['GM'], config['template']['WM'], config['template']['CSF']],                   gaussians_per_class = [2, 2, 2, 4],                   affine_regularization = 'subj',                   warping_regularization = 1,                   bias_regularization = 1e-5,                   bias_fwhm = 30,                   sampling_distance = 3)
seg.inputs.data = '/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/rsub-01_ses-01_run-01_T1w.nii'
seg.run()


# In[172]:


#newseg = spm.NewSegment(affine_regularization='subj', \
#                        channel_info=(1e-5, 30, (False, False)),\
#                        sampling_distance=3)
#tissue1 = ((config['template']['GM'], 1), 2, (True,True), (False, False))
#tissue2 = ((config['template']['WM'], 1), 2, (True,True), (False, False))
#tissue3 = ((config['template']['CSF'], 1), 4, (True,False), (False, False))
#newseg.inputs.tissues = [tissue1, tissue2, tissue3]
#newseg.inputs.channel_files = '/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/rsub-01_ses-01_run-01_T1w.nii'
#newseg.run()


# In[267]:


V = nib.load('/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/mrsub-01_ses-01_run-01_T1w.nii')
V1 = nib.load('/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/c1rsub-01_ses-01_run-01_T1w.nii')
V2 = nib.load('/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/c2rsub-01_ses-01_run-01_T1w.nii')
V3 = nib.load('/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/c3rsub-01_ses-01_run-01_T1w.nii')


# In[268]:


data = V.get_data()
datax = np.zeros(V.shape)
datay = V1.get_data() + V2.get_data() + V3.get_data()


# In[269]:


datay_smooth = ndimage.gaussian_filter(datay, 2)


# In[270]:


datay_binary = np.where(datay_smooth>0.4, 1, 0)


# In[271]:


datay_binary_fill = ndimage.binary_dilation(datay_binary)


# In[272]:


data_skull = data * datay_binary_fill
data_skull_img = nib.Nifti1Image(data_skull, affine=V.affine)


# In[273]:


nib.save(img=data_skull_img, filename='/home/neuro/output/EST06-derivatives/sub-01/ses-01/anat/emrsub-01_ses-01_run-01_T1w.nii')


# In[ ]:




